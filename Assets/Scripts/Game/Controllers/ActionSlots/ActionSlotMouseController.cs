﻿using Game.Controllers.Utility;
using UnityEngine;

namespace Game.Controllers
{
    class ActionSlotMouseController : MonoBehaviour
    {
        public MouseEventPasser[] framePassers;
        public MouseEventPasser imagePasser;

        public ActionSlotController mainController;
        private GameObject MainGameObject => mainController.gameObject;

        private Vector3 screenPoint;
        private Vector3 offset;

        public void Start()
        {
            for (var i = 0; i < framePassers.Length; i++)
                framePassers[i].Assign(StartDraging, DragActionSlot, DropActionSlot);

            imagePasser.Assign(mainController.CallGui, null, null);
        }

        void StartDraging()
        {
            screenPoint = Camera.main.WorldToScreenPoint(MainGameObject.transform.position);
            offset = MainGameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        }

        void DragActionSlot()
        {
            var curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            MainGameObject.transform.position = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset + new Vector3(0, 0.2f, 0);

        }

        private void DropActionSlot()
        {
            var curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            MainGameObject.transform.position = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        }
    }
}
