﻿using Game.Controllers.Ui;
using Game.Controllers.Utility;
using Game.DataModels;
using Game.DataModels.Recepies;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using Utilities.Colors;

namespace Game.Controllers
{
    public class ActionSlotController : MonoBehaviour, IGuiCaller
    {
        /// <summary>
        /// Public only for debbug reason please change me to private later :)
        /// </summary>
        public ActionSlot _data = null;
        public ActionSlot Data {
            private set {
                if (_data is null)
                    _data = ScriptableObject.CreateInstance<ActionSlot>();
                if(!(value is null))
                    _data = (ActionSlot)value.Clone();
            }
            get => _data;
        }
        [Header("Prefabs")]
        public GameObject uiPrefab = null;
        public GameObject symbolPrefab = null;
        [Header("Instanced")]
        public Transform icon = null;
        public Transform[] symbolsGroup = null;
        public MouseEventPasser mouseEventClick = null;

        public List<Card> cards = new List<Card>();

        //Internal
        private GameObject uiInstance = null;
        public int Age { get; private set; } = 0;

        #region LifeCycle
        public void Init(ActionSlot data)
        {
            Data = data;
            SetIcon(Data.icon);
            SpawnSymbols(Data.lifeTime);
            ColorSymbols(Data.lifeTime);
            mouseEventClick.Assign(null, null, CallGui);
            GameController.AddActionSlot(this);
        }

        public void Init(ActionSlot data, int age = 0)
        {
            Data = data;
            Age = age;
            SetIcon(Data.icon);
            SpawnSymbols(Data.lifeTime);
            ColorSymbols(Data.lifeTime);
            mouseEventClick.Assign(null, null, CallGui);
            GameController.AddActionSlot(this);
        }

        public void NextTurn()
        {
            Age++;
            if (Age >= Data.lifeTime)
            {
                 Destroy();
                return;
            }
            ColorSymbols(Data.lifeTime);
            SealCardSlots();
        }

        private void Destroy()
        {
            HandleHungrySlots(GameController.GetPlayerCards());
            GameController.RemoveActionSlot(this);
        }
        #endregion

        #region ResolveFunction
        public List<Recepie> GetFittingRecepies()
        {
            var accesableList = _data.possibleRecepies.Where(x => x.CanBeAccesed(cards)).ToList();
            if (accesableList.Count == 0)
                return new List<Recepie>(new Recepie[] { _data.nullRecepie });
            else return accesableList;
        }

        private void HandleHungrySlots(List<(Card, GameObject)> playerCards)
        {
            for (var i = 0; i < Data.cardSockeds.Length; i++)
            {
                if (Data.cardSockeds[i] is HungryCardSocked)
                {
                    if (playerCards.Any(x => Data.cardSockeds[i].CanBePlaced(x.Item1)))
                    {
                        var cardToTransfer = playerCards.First(x => Data.cardSockeds[i].CanBePlaced(x.Item1));
                        var toSave = cards;
                        cards.Add(cardToTransfer.Item1);
                        Destroy(cardToTransfer.Item2);
                    }
                }
            }
        }

        public void SealCardSlots()
        {
            for (var i = 0; i < cards.Count; i++)
                if (Data.cardSockeds[i].currentState == CardSocketState.CardSlot || Data.cardSockeds[i].currentState == CardSocketState.Back)
                {
                    switch (Data.cardSockeds[i].currentState)
                    {
                        case CardSocketState.Back:
                            Data.cardSockeds[i].currentState = CardSocketState.SealdBack;
                            break;
                        case CardSocketState.CardSlot:
                            Data.cardSockeds[i].currentState = CardSocketState.SealdCardSlot;
                            break;
                    }
                }
        }
        #endregion

        #region VisualElements

        private void SetIcon(Texture2D sprite)
        {
            var materialToChange = Instantiate(icon.GetComponent<MeshRenderer>().material, icon);
            icon.GetComponent<MeshRenderer>().material = materialToChange;

            materialToChange.EnableKeyword("_BaseMap");
            materialToChange.EnableKeyword("_EMISSION");

            materialToChange.SetTexture(Shader.PropertyToID("_BaseMap"), sprite);
            materialToChange.SetTexture(Shader.PropertyToID("_EmissionMap"), sprite);
        }

        private void SpawnSymbols(int numOfSymbols)
        {
            const float symbolSize = 0.5f;
            var startXPosition = (numOfSymbols <= 1) ? 0 : ((numOfSymbols - 1) * symbolSize) / -2f;
            for (var i = 0; i < numOfSymbols; i++)
            {
                for (var j = 0; j < symbolsGroup.Length; j++)
                {
                    var spawnedSymbol = Instantiate(symbolPrefab, symbolsGroup[j]);
                    spawnedSymbol.transform.localPosition = new Vector3(startXPosition, 0, 0);
                }
                startXPosition += symbolSize;
            }
        }

        private void ColorSymbols(int numOfSymbols)
        {
            for (var i = 0; i < numOfSymbols; i++)
            {
                for (var j = 0; j < symbolsGroup.Length; j++)
                {
                    if (symbolsGroup[j].childCount > i)
                        continue;
                    var symbolController = symbolsGroup[j].GetChild(i).GetComponent<AbstractToneElement>();
                    if (i < Age)
                        symbolController.AssignColor(2);
                    else if (i == Age)
                        symbolController.AssignColor(1);
                    else
                        symbolController.AssignColor(0);
                }
            }
        }
        #endregion

        #region Ui
        public void CallGui()
        {
            if (uiInstance is null)
            {
                uiInstance = Instantiate(uiPrefab);
                uiInstance.transform.position = new Vector3(0, 1, 0);
                uiInstance.GetComponent<ActionSlotUiController>().Init(this);
                GameController.AddActiveUi(this);
                uiInstance.SetActive(true);
            }
        }

        public void DestroyGui()
        {
            if (!(uiInstance is null))
            {
                uiInstance.GetComponent<ActionSlotUiController>().Exit();
                GameController.RemoveActiveUi(this);
                uiInstance = null;
            }
        }
        #endregion
    }
}

