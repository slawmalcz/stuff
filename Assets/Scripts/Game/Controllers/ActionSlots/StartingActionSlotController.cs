﻿using Utilities;

namespace Game.Controllers
{
    public class StartingActionSlotController : ActionSlotController
    {
        private void Start() => Init(ResourceManager.GetActionSlotById(_data.id));
    }
}
