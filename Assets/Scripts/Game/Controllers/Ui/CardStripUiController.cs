﻿using Game.DataModels;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controllers.Ui
{
    public class CardStripUiController : MonoBehaviour
    {
        public Animator animator;
        public CardSockedController[] sockeds;
        public GameObject[] triggers;

        public GameObject defaultCardPrefab = null;

        public List<Card> HoldedCards {
            get {
                var ret = new List<Card>();
                for (var i = 0; i < sockeds.Length; i++)
                    if (sockeds[i].cardSocked.childCount > 0)
                        ret.Add(sockeds[i].HoldedCardController.cardData);
                return ret;
            }
        }

        public void Populate(Dictionary<int, Card> cards, Dictionary<int, CardSocked> frames)
        {
            var toDisplay = 0;
            foreach (var element in frames)
            {
                sockeds[element.Key].CurrentState = CardSocketState.CardSlot;
                if (element.Value.currentState == CardSocketState.Back
                    || element.Value.currentState == CardSocketState.CardSlot
                    || element.Value.currentState == CardSocketState.SealdBack
                    || element.Value.currentState == CardSocketState.SealdCardSlot)
                    toDisplay++;
            }
            for(var i = 0; i < sockeds.Length; i++)
            {
                if (i >= sockeds.Length / 2)
                    FlipCards(i);
            }
            foreach (var element in cards)
            {
                var cardControllerGo = Instantiate(defaultCardPrefab, sockeds[element.Key].cardSocked);
                cardControllerGo.GetComponent<CardController>().Init(element.Value);
            }
            if (toDisplay > 6)
                foreach (var element in triggers)
                    element.SetActive(true);

        }

        #region Animator Controll
        public void SwipeRight() => animator.SetTrigger("SwipeRight");
        public void SwipeLeft() => animator.SetTrigger("SwipeLeft");

        [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeQuality",
                                                         "IDE0051:Remove unused private members",
                                                         Justification = "Private member is used as caller in Animator Calss")]
        private void FlipCards(int num)
        {
            if (num >= sockeds.Length)
                Debug.Log("Error");
            var toChange = sockeds[num].GetComponent<CardSockedController>();
            switch (toChange.CurrentState)
            {
                case CardSocketState.Back:
                    toChange.CurrentState = CardSocketState.CardSlot;
                    break;
                case CardSocketState.SealdBack:
                    toChange.CurrentState = CardSocketState.SealdCardSlot;
                    break;
                case CardSocketState.BackEmpty:
                    toChange.CurrentState = CardSocketState.Empty;
                    break;
                case CardSocketState.CardSlot:
                    toChange.CurrentState = CardSocketState.Back;
                    break;
                case CardSocketState.SealdCardSlot:
                    toChange.CurrentState = CardSocketState.SealdBack;
                    break;
                case CardSocketState.Empty:
                    toChange.CurrentState = CardSocketState.BackEmpty;
                    break;
            }
        }
        #endregion
    }
}
