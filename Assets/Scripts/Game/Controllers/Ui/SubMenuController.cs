﻿using Menu;
using Save;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Controllers.Ui
{
    public class SubMenuController : MonoBehaviour, IGuiCaller
    {
        [Header("Components")]
        public GameObject mainPanel = null;
        public GameObject loadTab = null;
        public GameObject saveConfirmationInfo = null;

        public void Start() => ResetTabs();

        public void ResetTabs()
        {
            mainPanel.SetActive(true);
            loadTab.SetActive(false);
        }

        public void OnSaveClick()
        {
            var saveFile = GameController.GenerateSaveFile();
            SaveManager.Instance.SaveCurrentSaveFile(saveFile);
            saveConfirmationInfo.SetActive(true);
        }

        public void OnLoadClick()
        {
            ResetTabs();
            mainPanel.SetActive(false);
            loadTab.SetActive(true);
            var listOfSaveSlots = loadTab.transform.Find("Content").Find("SaveSlots").GetComponentsInChildren<SaveFileSltoUiController>();
            for (var i = 0; i < listOfSaveSlots.Length; i++)
            {
                var displayData = SaveManager.Instance.ExtractSaveFileInfo(i);
                listOfSaveSlots[i].GetComponent<SaveFileSltoUiController>().Init(i, displayData.Item1, displayData.Item2, Load);
            }
        }

        private void Load(int sloatNumber) => Load(SaveManager.Instance.ExtractSaveFile(sloatNumber));

        private void Load(SaveFile saveFile)
        {
            var createdObject = new GameObject("InformationPasser");
            createdObject.AddComponent<InformationPasser>().Init(saveFile);
            SceneManager.LoadScene("GameScene");
        }

        public void OnExitClick() => SceneManager.LoadScene("MenuScene");

        public void OnBackClick() => DestroyGui();

        public void CallGui()
        {
            GameController.AddActiveUi(this);
            gameObject.SetActive(true);
        }

        public void DestroyGui()
        {
            GameController.RemoveActiveUi(this);
            gameObject.SetActive(false);
        }
    }
}

