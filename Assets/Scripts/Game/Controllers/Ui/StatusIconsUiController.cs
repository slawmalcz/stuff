﻿using Game.DataModels;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Controllers.Ui
{
    public class StatusIconsUiController : MonoBehaviour
    {
        public Transform[] slots;
        public GameObject statusIconPrefab;

        public void Diplay(Dictionary<CardsType, int> dataToDisplay)
        {
            var counter = 0;
            foreach (var element in dataToDisplay)
            {
                var createdObject = Instantiate(statusIconPrefab, slots[counter]);
                var controlller = createdObject.GetComponent<StatusIconController>();
                controlller.quantityField.text = element.Value.ToString();
                var innerMat = controlller.icon.GetComponent<MeshRenderer>().material;
                innerMat.EnableKeyword("_BaseMap");
                innerMat.mainTexture = element.Key.groupIcon;
                counter++;
            }
        }
    }
}
