﻿namespace Game.Controllers.Ui
{
    public interface IGuiCaller
    {
        void CallGui();
        void DestroyGui();
    }
}
