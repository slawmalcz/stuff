﻿using Game.DataModels;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Game.Controllers.Ui
{
    public class ActionSlotUiController : MonoBehaviour
    {
        private ActionSlotController caller = null;
        [Header("Text Fields")]
        public TextMeshPro title = null;
        public TextMeshPro header = null;
        public TextMeshPro description = null;

        [Header("Slots Components")]
        public Transform iconPlate = null;
        public CardStripUiController cardStrip = null;
        public StatusIconsUiController StatusIconUiController = null;

        #region Initialisation
        public void Init(ActionSlotController caller)
        {
            this.caller = caller;
            SetTexts(caller);
            SetIcon(caller);
            ExtractData(caller, out var preperedCardData, out var preperedFramesData);
            cardStrip.Populate(preperedCardData, preperedFramesData);
            var listofStatus = GetStatusIcons(preperedCardData, preperedFramesData);
            StatusIconUiController.Diplay(listofStatus);
        }

        private Dictionary<CardsType, int> GetStatusIcons(Dictionary<int, Card> preperedCardData,
                                                          Dictionary<int, CardSocked> preperedFramesData)
        {
            var ret = new Dictionary<CardsType, int>();
            foreach (var element in preperedCardData)
                if (preperedFramesData.ContainsKey(element.Key) &&
                   (preperedFramesData[element.Key].currentState == CardSocketState.SealdCardSlot ||
                    preperedFramesData[element.Key].currentState == CardSocketState.SealdBack))
                {
                    foreach (var line in CardTypeLibrary.GetCardTypes(element.Value))
                    {
                        if (ret.ContainsKey(line))
                            ret[line]++;
                        else
                            ret.Add(line, 1);
                    }
                }
            return ret;
        }

        private void ExtractData(ActionSlotController caller,
                                 out Dictionary<int, Card> preperedCardData,
                                 out Dictionary<int, CardSocked> preperedFramesData)
        {
            preperedCardData = new Dictionary<int, Card>();
            preperedFramesData = new Dictionary<int, CardSocked>();
            for (var i = 0; i < caller.cards.Count; i++)
                preperedCardData.Add(i, caller.cards[i]);
            for (var i = 0; i < caller.Data.cardSockeds.Length; i++)
                if (!(caller.Data.cardSockeds[i] is null))
                    preperedFramesData.Add(i, caller.Data.cardSockeds[i]);
        }

        private void SetIcon(ActionSlotController caller)
        {
            var materialToChange = Instantiate(iconPlate.GetComponent<MeshRenderer>().material, iconPlate.transform);
            iconPlate.GetComponent<MeshRenderer>().material = materialToChange;

            materialToChange.EnableKeyword("_BaseMap");
            materialToChange.EnableKeyword("_EMISSION");

            materialToChange.SetTexture(Shader.PropertyToID("_BaseMap"), caller.Data.icon);
            materialToChange.SetTexture(Shader.PropertyToID("_EmissionMap"), caller.Data.icon);
        }

        private void SetTexts(ActionSlotController caller)
        {
            title.text = caller.Data.actionName;
            header.text = caller.Data.header;
            description.text = caller.Data.description;
        }
        #endregion
        
        public void ButtonExit() => caller.DestroyGui();

        public void Exit()
        {
            Save();
            Destroy(gameObject);
        }

        public void Save() => caller.cards = cardStrip.HoldedCards;
    }
}

