﻿using TMPro;
using UnityEngine;

namespace Game.Controllers.Ui
{
    public class StatusIconController : MonoBehaviour
    {
        public TextMeshPro quantityField = null;
        public Transform icon = null;
    }
}
