﻿using UnityEngine;

namespace Game.Controllers.Ui
{
    public class CardSockedDecorationUiController : MonoBehaviour
    {
        [Range(0, 1)] 
        public float wealth = 0;
        public GameObject[] decorations = null;

        public void Start() => ResetDecorations();

        private void ResetDecorations()
        {
            for (var i = 0; i < decorations.Length; i++)
                decorations[i].SetActive(false);

            var randomSequence = new System.Random(Time.frameCount);
            for (var i = 0; i < decorations.Length; i++)
                if (randomSequence.NextDouble() < wealth)
                    decorations[i].SetActive(true);
        }
    }
}
