﻿using Game.Controllers.Utility;
using Game.DataModels;
using System;
using UnityEngine;

namespace Game.Controllers.Ui
{
    public class CardSockedController : MonoBehaviour
    {
        private CardSocketState _currentState = CardSocketState.Empty;
        public CardSocketState CurrentState {
            get => _currentState;
            set {
                _currentState = value;
                SetModel(value);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style",
                                                         "IDE0066:Convert switch statement to expression",
                                                         Justification = " Not aviabble in currently supported Unity version")]
        public bool IsSeald {
            get {
                switch (CurrentState)
                {
                    case CardSocketState.CardSlot:
                    case CardSocketState.BackEmpty:
                    case CardSocketState.Empty:
                    case CardSocketState.Back:
                        return false;
                    case CardSocketState.SealdCardSlot:
                    case CardSocketState.SealdBack:
                        return true;
                    default:
                        throw new NotImplementedException($"There is no recognition for ({CurrentState.ToString()}) as seald or not");
                }
            }
        }

        public CardController HoldedCardController {
            get {
                if (transform.childCount <= 0)
                    return null;
                return transform.GetComponentInChildren<CardController>();
            }
        }

        [Header("Group Elements")]
        public GameObject cardSlot;
        public GameObject sealdCardSlot;
        public GameObject empty;
        public GameObject back;
        public GameObject backEmpty;
        public Transform cardSocked;

        private ColliderDetectionPasser DetectionCollider => cardSocked.GetComponent<ColliderDetectionPasser>();

        public void Awake() => SetModel(_currentState);
        void Start() => _ = DetectionCollider.Assign(OnCardEnter, null, OnCardExit);

        private void SetModel(CardSocketState value)
        {
            //Deactivate all
            cardSlot.SetActive(false);
            sealdCardSlot.SetActive(false);
            empty.SetActive(false);
            back.SetActive(false);
            backEmpty.SetActive(false);
            back.SetActive(false);

            switch (value)
            {
                case (CardSocketState.CardSlot):
                    cardSlot.SetActive(true);
                    cardSocked.gameObject.SetActive(true);
                    break;
                case CardSocketState.SealdCardSlot:
                    sealdCardSlot.SetActive(true);
                    cardSocked.gameObject.SetActive(true);
                    break;
                case CardSocketState.Empty:
                    empty.SetActive(true);
                    cardSocked.gameObject.SetActive(false);
                    break;
                case CardSocketState.Back:
                    back.SetActive(true);
                    cardSocked.gameObject.SetActive(false);
                    break;
                case CardSocketState.BackEmpty:
                    backEmpty.SetActive(true);
                    cardSocked.gameObject.SetActive(false);
                    break;
                case CardSocketState.SealdBack:
                    back.SetActive(true);
                    cardSocked.gameObject.SetActive(false);
                    break;
            }
        }

        #region Card Detection Controll
        private void OnCardEnter(Collider other)
        {
            if (other.gameObject.GetComponent<CardController>() is null)
                return;
            other.transform.parent = cardSocked.transform;
            other.gameObject.transform.localPosition = Vector3.zero;
        }
        private void OnCardExit(Collider other)
        {
            if (HoldedCardController == other.gameObject.GetComponent<CardController>())
                other.transform.parent = GameController.GetPlayerHand();
        }
        #endregion
    }
}
