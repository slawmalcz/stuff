﻿using UnityEngine;
using UnityEngine.Events;
using Utilities.Colors;

namespace Game.Controllers.Ui
{
    public class Button3DController : MonoBehaviour
    {
        public Transform pillow = null;
        public Transform fill = null;

        public UnityEvent onClick;

        private bool isPushed = false;
        private Color fillColor = Color.white;
        private Color fillToneColor = Color.gray;

        public void Awake()
        {
            fillColor = FindObjectOfType<ColorPalletsController>().GetMixedColors().GetColor(CollorPanetOptions.ButtonTone, 1);
            fillToneColor = FindObjectOfType<ColorPalletsController>().GetMixedColors().GetColor(CollorPanetOptions.ButtonTone, 0);
            fill.GetComponent<SpriteRenderer>().color = fillColor;
        }

        #region Mouse Events
        public void OnMouseDown()
        {
            isPushed = true;
            pillow.localEulerAngles += new Vector3(0, 180, 0);
            fill.localEulerAngles += new Vector3(0, 180, 0);

            fill.GetComponent<SpriteRenderer>().material.color = fillToneColor;
        }

        private void OnMouseExit()
        {
            if (isPushed)
            {
                pillow.localEulerAngles -= new Vector3(0, 180, 0);
                fill.localEulerAngles -= new Vector3(0, 180, 0);

                fill.GetComponent<SpriteRenderer>().material.color = fillColor;
                isPushed = false;
            }
        }

        public void OnMouseUpAsButton()
        {
            OnMouseExit();
            onClick.Invoke();
        }
        #endregion
    }
}
