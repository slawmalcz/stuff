﻿using TMPro;
using UnityEngine;
using Utilities;

namespace Game.Controllers.Ui
{
    public class PackageUiController : MonoBehaviour
    {
        private PackageController caller = null;
        [Header("Text")]
        public TextMeshPro title = null;
        public TextMeshPro header = null;
        public TextMeshPro description = null;
        [Header("Components")]
        public Transform spawnPlate = null;
        [Header("Prefabs")]
        public GameObject cardPrefab = null;
        public GameObject actionSlotPrefab = null;

        #region Initialization
        public void Init(PackageController caller)
        {
            this.caller = caller;
            SetTexts(caller);
            SpawnCards(caller);
            SpawnActionSlots(caller);
        }

        private void SpawnActionSlots(PackageController caller)
        {
            var sloatsToSpawn = caller.output.GetActionSlotReward();
            for (var i = 0; i < sloatsToSpawn.Length; i++)
                Instantiate(actionSlotPrefab, spawnPlate).GetComponent<ActionSlotController>().Init(ResourceManager.GetActionSlotById(sloatsToSpawn[i].id));
        }

        private void SpawnCards(PackageController caller)
        {
            var cardsToSpawn = caller.output.GetCardReward();
            for (var i = 0; i < cardsToSpawn.Length; i++)
                Instantiate(cardPrefab, spawnPlate).GetComponent<CardController>().Init(cardsToSpawn[i]);
        }

        private void SetTexts(PackageController caller)
        {
            title.text = caller.output.name;
            header.text = caller.output.header;
            description.text = caller.output.descriptions;
        }
        #endregion

        public void Exit() => Destroy(gameObject);

        public void ButtonExit() => caller.DestroyGui();
    }
}
