﻿using Game.DataModels;
using TMPro;
using UnityEngine;

namespace Game.Controllers
{
    public class CardController : MonoBehaviour
    {
        public Card cardData = null;
    
        public TextMeshPro nameText = null;
        public GameObject iconObject = null;


        public void Init(Card data)
        {
            cardData = data;
            nameText.text = cardData.name;
            nameText.renderer.sortingOrder = 0;
            iconObject.GetComponent<SpriteRenderer>().sprite = cardData.icon;
            GameController.AddCard(this);
        }

        public void NextTurn()
        {
            //TODO::Mayby some time later
        }

        #region MouseController
        private Vector3 screenPoint;
        private Vector3 offset;

        void OnMouseDown()
        {
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

            gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }

        void OnMouseDrag()
        {
            var curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

            var curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset + new Vector3(0, 1.5f, 0);
            transform.position = curPosition;
        }

        private void OnMouseUpAsButton()
        {
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
        #endregion
    }
}
