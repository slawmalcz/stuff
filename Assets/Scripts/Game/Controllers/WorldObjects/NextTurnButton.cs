﻿using System.Collections.Generic;
using UnityEngine;
using Utilities.Colors;

namespace Game.Controllers
{
    public class NextTurnButton : MonoBehaviour
    {

        public float timeToHold = 4;
        public List<AbstractToneElement> symbols = null;

        private bool isClicked = false;
        private float timeSinceLastActivation = 0;

        public void Update()
        {
            if (isClicked)
            {
                timeSinceLastActivation += Time.deltaTime;
                if (timeSinceLastActivation >= timeToHold)
                    Activate();
                else
                    ColorSymbols();
            }
        }

        private void ColorSymbols()
        {
            var timeSlice = Mathf.FloorToInt((timeSinceLastActivation / timeToHold) * symbols.Count);
            for (var i = 0; i < symbols.Count; i++)
            {
                if (i < timeSlice)
                    ((ToneChangeSymbol)symbols[i]).AssignColor(2);
                else if (i == timeSlice)
                    ((ToneChangeSymbol)symbols[i]).AssignColor(1);
                else
                    ((ToneChangeSymbol)symbols[i]).AssignColor(0);
            }
        }

        private void Activate()
        {
            timeSinceLastActivation = 0;
            isClicked = false;
            ResetAllSymbols();
            GameController.NextTurn();
        }

        private void ResetAllSymbols()
        {
            for (var i = 0; i < symbols.Count; i++)
                symbols[i].AssignColor(0);
        }

        #region MouseEvents
        public void OnMouseDown() => isClicked = true;

        public void OnMouseExit()
        {
            if (isClicked)
            {
                isClicked = false;
                timeSinceLastActivation = 0;
                ResetAllSymbols();
            }
        }
        public void OnMouseUpAsButton()
        {
            if (isClicked)
            {
                isClicked = false;
                timeSinceLastActivation = 0;
                ResetAllSymbols();
            }
        }
        #endregion
    }
}
