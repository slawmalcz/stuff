﻿using Game.Controllers.Ui;
using Game.DataModels.Recepies;
using UnityEngine;

namespace Game.Controllers
{
    public class PackageController : MonoBehaviour, IGuiCaller
    {
        public Recepie output = null;
        public GameObject guiPrefab;
        private GameObject guiInstance = null;

        public void OnMouseDown() => CallGui();

        public void CallGui()
        {
            if (guiInstance is null)
            {
                guiInstance = Instantiate(guiPrefab);
                guiInstance.transform.position = new Vector3(0, 1, 0);
                guiInstance.GetComponent<PackageUiController>().Init(this);
                GameController.AddActiveUi(this);
                guiInstance.SetActive(true);
            }
        }

        public void DestroyGui()
        {
            if (!(guiInstance is null))
            {
                guiInstance.GetComponent<PackageUiController>().Exit();
                guiInstance = null;
                Destroy(gameObject);
            }
        }
    }
}
