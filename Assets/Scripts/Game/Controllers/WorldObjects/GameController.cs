﻿using Game.Controllers.Ui;
using Game.DataModels;
using Game.DataModels.Recepies;
using Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utilities;
using Utilities.Colors;

namespace Game.Controllers
{
    public static class GameController
    {
        private static WolrdController _instance = null;
        private static WolrdController Instance {
            get {
                if (_instance is null || _instance.IsValid())
                    _instance = GameObject.FindObjectOfType<WolrdController>();
                return _instance;
            }
            set {
                _instance = value;
            }
        }

        public static void AddToCheduler(Recepie selectedRecepie) => Instance.spawnScheduler.AddSchedulerElement(selectedRecepie.executionTime,selectedRecepie);

        public static List<(Card, GameObject)> GetPlayerCards()
        {
            var playerCards = new List<(Card, GameObject)>();
            for (var j = 0; j < Instance.playerCarsd.childCount; j++)
            {
                var cardInstance = Instance.playerCarsd.GetChild(j);
                playerCards.Add((cardInstance.GetComponent<CardController>().cardData, cardInstance.gameObject));
            }

            return playerCards;
        }

        public static void CallGameOver()
        {
            SceneManager.LoadScene("MenuScene");
        }

        public static void NextTurn() => Instance.NextTurn();
        public static string GetName() => Instance.gameName;
        public static void SetName(string value) => Instance.gameName = value;

        public static void AddCard(CardController cardController)
        {
            Instance.onNewTurnUpdate.Add(cardController.NextTurn);
            cardController.transform.parent = Instance.playerCarsd;
        }

        public static void AddActionSlot(ActionSlotController action)
        {
            Instance.onNewTurnUpdate.Add(action.NextTurn);
            action.transform.parent = Instance.playerActions;
            action.transform.localPosition = Vector3.Scale(action.transform.localPosition, new Vector3(1, 0, 1));
        }

        public static void RemoveActionSlot(ActionSlotController action)
        {
            Instance.onNewTurnUpdate.Remove(action.NextTurn);
            foreach(var selectedRecepie in action.GetFittingRecepies())
            {
                if (selectedRecepie is null)
                    continue;
                if (selectedRecepie.isEnding)
                {
                    CallGameOver();
                    break;
                }
                AddToCheduler(selectedRecepie);
            }
            Instance.toDestroyInThisTurn.Add(action.gameObject);
        }

        public static void AddActiveUi(IGuiCaller activeUi)
        {
            Instance.guiCallers.Add(activeUi);
        }

        public static void RemoveActiveUi(IGuiCaller activeUi)
        {
            Instance.guiCallers.Remove(activeUi);
        }

        internal static void TryAssign(WolrdController wolrdController)
        {
            if (_instance is null)
                Instance = wolrdController;
        }

        public static Transform GetPlayerHand() => Instance.playerCarsd;

        // Save Load
        public static SaveFile GenerateSaveFile()
        {
            var actionSlotsData = Instance.playerActions.GetComponentsInChildren<ActionSlotController>();
            var actionDataOutput = new List<(int, Vector3, int[],int)>();
            foreach (var row in actionSlotsData)
                actionDataOutput.Add((row.Data.id, row.transform.position,row.cards.Select(x=>x.id).ToArray(), row.Age));

            var cardsData = Instance.playerCarsd.GetComponentsInChildren<CardController>();
            var cardsDataOutput = new List<(int, Vector3)>();
            foreach (var row in cardsData)
                cardsDataOutput.Add((row.cardData.id, row.transform.position));
            return new SaveFile(Instance.loadedFromSlot, GetName(), Instance.currentTurn, actionDataOutput, cardsDataOutput, Instance.spawnScheduler.PrepearSaveData());
        }

        public static void LoadGameFile(SaveFile saveFile)
        {
            SetName(saveFile.name);
            Instance.currentTurn = saveFile.turn;
            Instance.loadedFromSlot = saveFile.slot;
            foreach (var row in saveFile.ActionSlotsData)
            {
                var actionSlot = ResourceManager.GetActionSlotById(row.Item1);
                var element = GameObject.Instantiate(Instance.actionSlotPrefab, row.Item2, Quaternion.identity)
                    .GetComponent<ActionSlotController>();
                element.Init(actionSlot,row.Item4);
                element.cards.AddRange(row.Item3.Select(x => ResourceManager.GetCardById(x)).ToList());
            }
            foreach (var row in saveFile.CardData)
            {
                var createdGo = GameObject.Instantiate(Instance.cardPrefab, row.Item2, Quaternion.identity);
                createdGo.GetComponent<CardController>().Init(ResourceManager.GetCardById(row.Item1));
            }
            if (Instance.spawnScheduler is null)
                Instance.spawnScheduler = new Utility.RecepiesSpawnScheduler();
            Instance.spawnScheduler.LoadData(saveFile.Schedulers);
        }
    }
}