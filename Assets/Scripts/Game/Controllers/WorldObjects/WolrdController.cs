﻿using Game.Controllers.Ui;
using Game.Controllers.Utility;
using Game.DataModels.Recepies;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Controllers
{
    public class WolrdController : MonoBehaviour
    {
        public int loadedFromSlot = 0;
        public string gameName = "Testing Ground";
        public int currentTurn = 0;

        public List<Action> onNewTurnUpdate = new List<Action>();
        public List<GameObject> toDestroyInThisTurn = new List<GameObject>();
        public List<IGuiCaller> guiCallers = new List<IGuiCaller>();

        public GameObject packagePrefab;
        public Transform packageSpawner;
        public Transform playerCarsd;
        public Transform playerActions;
        public SubMenuController subMenu;

        public GameObject actionSlotPrefab;
        public GameObject cardPrefab;

        [HideInInspector]
        public RecepiesSpawnScheduler spawnScheduler;

        public void Awake()
        {
            GameController.TryAssign(this);
            spawnScheduler = new RecepiesSpawnScheduler();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (guiCallers.Count > 0)
                {
                    var toClose = guiCallers.First();
                    guiCallers.Remove(toClose);
                    toClose.DestroyGui();
                }
                else
                {
                    subMenu.CallGui();
                }
            }
            guiCallers = guiCallers.Where(x => !(x is null)).ToList();
        }

        internal void NextTurn()
        {
            currentTurn++;
            PerformActionSlotsTurn();
            SpawnRequiredPackages();
            DeleteActionSlots();
            spawnScheduler.NextTurn();
        }

        private void DeleteActionSlots()
        {
            for(var i = 0;i < toDestroyInThisTurn.Count; i++)
                GameObject.Destroy(toDestroyInThisTurn[i]);
        }

        private void SpawnRequiredPackages()
        {
            var cards = playerCarsd.GetComponentsInChildren<CardController>().Select(x => x.cardData).ToList();
            var slots = playerActions.GetComponentsInChildren<ActionSlotController>().Select(x => x.Data).ToList();
            foreach (var element in spawnScheduler.GetTurnSpawnRecepies(cards, slots))
            {
                var spawnedPackage = Instantiate(packagePrefab, packageSpawner);
                spawnedPackage.transform.parent = playerCarsd;
                spawnedPackage.GetComponent<PackageController>().output = element;
            }
        }

        private void PerformActionSlotsTurn()
        {
            var actionSlotUpdates = onNewTurnUpdate.ToArray();
            for (var i = 0; i < actionSlotUpdates.Length; i++)
                actionSlotUpdates[i].Invoke();
        }

        internal bool IsValid() => !(onNewTurnUpdate is null) && !(guiCallers is null) && !(packagePrefab is null) && !(packageSpawner is null) &&
                   !(playerCarsd is null) && !(playerActions is null) && !(actionSlotPrefab is null) && !(cardPrefab is null);
    }
}
