﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Controllers.Utility
{
    public partial class ColliderDetectionPasser : MonoBehaviour
    {
        private List<ColliderHandlerElement> handlerList = new List<ColliderHandlerElement>();

        public int Assign(Action<Collider> OnEnter, Action<Collider> OnStay, Action<Collider> OnExit)
        {
            var handler = new ColliderHandlerElement(OnEnter, OnStay, OnExit);
            handlerList.Add(handler);
            return handler.id;
        }

        public void Remove(int handlerId)
        {
            try
            {
                handlerList.Remove(handlerList.First(x => x.id == handlerId));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
                return;
            }
        }

        private void OnTriggerEnter(Collider other) => handlerList.ForEach(x => x.CallOnEnter(other));
        private void OnTriggerStay(Collider other) => handlerList.ForEach(x => x.CallStay(other));
        private void OnTriggerExit(Collider other) => handlerList.ForEach(x => x.CallExit(other));
    }
}

