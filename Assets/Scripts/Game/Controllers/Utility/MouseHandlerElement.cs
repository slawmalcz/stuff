﻿using System;

namespace Game.Controllers.Utility
{
    internal class MouseHandlerElement
    {
        private static int hashListCounter = 0;

        public readonly int id = 0;
        public Action OnMouseDown = delegate { };
        public Action OnMouseDrag = delegate { };
        public Action OnMouseUpAsButton = delegate { };

        public MouseHandlerElement(Action OnMouseDown, Action OnMouseDrag, Action OnMouseUpAsButton)
        {
            id = GetNewId();

            if (!(OnMouseDown is null))
                this.OnMouseDown = OnMouseDown;
            if (!(OnMouseDrag is null))
                this.OnMouseDrag = OnMouseDrag;
            if (!(OnMouseUpAsButton is null))
                this.OnMouseUpAsButton = OnMouseUpAsButton;
        }

        public void CallOnMouseDown()
        {
            if (!(OnMouseDown is null))
                OnMouseDown.Invoke();
        }
        public void CallOnMouseDrag()
        {
            if (!(OnMouseDrag is null))
                OnMouseDrag.Invoke();
        }
        public void CallOnMouseUpAsButton()
        {
            if (!(OnMouseUpAsButton is null))
                OnMouseUpAsButton.Invoke();
        }

        private static int GetNewId()
        {
            hashListCounter++;
            return hashListCounter;
        }
    }
}
