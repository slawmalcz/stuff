﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Controllers.Utility
{
    public partial class MouseEventPasser : MonoBehaviour
    {
        private List<MouseHandlerElement> handlerList = new List<MouseHandlerElement>();

        public int Assign(Action OnMouseDown, Action OnMouseDrag, Action OnMouseUpAsButton)
        {
            var handler = new MouseHandlerElement(OnMouseDown, OnMouseDrag, OnMouseUpAsButton);
            handlerList.Add(handler);
            return handler.id;
        }

        public void Remove(int handlerId)
        {
            try
            {
                handlerList.Remove(handlerList.First(x => x.id == handlerId));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
                return;
            }
        }

        public void OnMouseDown() => handlerList.ForEach(x => x.CallOnMouseDown());
        public void OnMouseDrag() => handlerList.ForEach(x => x.CallOnMouseDrag());
        public void OnMouseUpAsButton() => handlerList.ForEach(x => x.CallOnMouseUpAsButton());
    }
}
