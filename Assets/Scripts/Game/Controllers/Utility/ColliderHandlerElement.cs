﻿using System;
using UnityEngine;

namespace Game.Controllers.Utility
{
    internal class ColliderHandlerElement
    {
        private static int hashListCounter = 0;

        public readonly int id = 0;
        public Action<Collider> OnEnter = delegate { };
        public Action<Collider> OnStay = delegate { };
        public Action<Collider> OnExit = delegate { };

        public ColliderHandlerElement(Action<Collider> OnEnter, Action<Collider> OnStay, Action<Collider> OnExit)
        {
            id = GetNewId();

            if (!(OnEnter is null))
                this.OnEnter = OnEnter;
            if (!(OnStay is null))
                this.OnStay = OnStay;
            if (!(OnExit is null))
                this.OnExit = OnExit;
        }

        public void CallOnEnter(Collider other)
        {
            if (!(OnEnter is null))
                OnEnter.Invoke(other);
        }
        public void CallStay(Collider other)
        {
            if (!(OnStay is null))
                OnStay.Invoke(other);
        }
        public void CallExit(Collider other)
        {
            if (!(OnExit is null))
                OnExit.Invoke(other);
        }

        private static int GetNewId()
        {
            hashListCounter++;
            return hashListCounter;
        }
    }
}

