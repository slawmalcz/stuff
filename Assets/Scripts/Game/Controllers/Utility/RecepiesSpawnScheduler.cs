﻿using Game.DataModels;
using Game.DataModels.Recepies;
using System.Collections.Generic;
using System.Linq;
using Utilities;

namespace Game.Controllers.Utility
{
    public class RecepiesSpawnScheduler
    {
        public class RecepiesSpawnSchedulerElement
        {
            public int turnToSpawn;
            public Recepie recepieToSpawn;

            public override string ToString() => $"{recepieToSpawn.name} : {turnToSpawn}";
        }

        private List<RecepiesSpawnSchedulerElement> recepieSpawnScheduler;

        public RecepiesSpawnScheduler() : this(new List<RecepiesSpawnSchedulerElement>()) { }

        public RecepiesSpawnScheduler(List<RecepiesSpawnSchedulerElement> schedulerElements) => recepieSpawnScheduler = schedulerElements;

        public List<Recepie> GetTurnSpawnRecepies(List<Card> cardsOnHand, List<ActionSlot> actionSlots)
        {
            var endingSchedules = recepieSpawnScheduler.Where(x => x.turnToSpawn == 0).ToList();
            var executableSchedules = endingSchedules.Where(x => x.recepieToSpawn.CanBeExecuted(cardsOnHand, actionSlots)).ToList();
            var selectedRecepie = executableSchedules.Select(x => x.recepieToSpawn).ToList();
            return selectedRecepie;
        }

        public void NextTurn()
        {
            for(var i = 0; i < recepieSpawnScheduler.Count; i++)
                recepieSpawnScheduler[i].turnToSpawn--;
            foreach (var element in recepieSpawnScheduler.Where(x => x.turnToSpawn < 0).ToList())
                recepieSpawnScheduler.Remove(element);
        }

        public void AddSchedulerElement(int turn, Recepie recepie)
        {
            recepieSpawnScheduler.Add(new RecepiesSpawnSchedulerElement { turnToSpawn = turn, recepieToSpawn = recepie });
        }

        public List<(int,int)> PrepearSaveData()
        {
            var ret = new List<(int, int)>();
            foreach (var row in recepieSpawnScheduler)
                ret.Add((row.recepieToSpawn.id, row.turnToSpawn));
            return ret;
        }

        internal void LoadData(List<(int, int)> schedulers)
        {
            foreach (var row in schedulers)
                AddSchedulerElement(row.Item1, ResourceManager.GetRecepieById(row.Item2));
        }
    }
}
