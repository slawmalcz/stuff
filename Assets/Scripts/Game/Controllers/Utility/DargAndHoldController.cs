﻿using UnityEngine;

namespace Game.Controllers.Utility
{
    public class DargAndHoldController : MonoBehaviour
    {
        public Transform mainObject = null;
        private Vector3 screenPoint;
        private Vector3 offset;

        public void OnMouseDown()
        {
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            offset = mainObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        }

        public void OnMouseDrag()
        {
            var curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            var curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            mainObject.position = curPosition;
        }
    }
}
