﻿using UnityEngine;

namespace Game.Controllers.Utility
{
    public class FillSizeController : MonoBehaviour
    {
        public Vector2 size = Vector2.one;
        public SpriteRenderer frameRenderer = null;
        public SpriteRenderer bcgRenderrer = null;
        public BoxCollider bcgBoxColider = null;
        public Transform contentContener = null;
        [Header("Corners")]
        public Transform LU_Corner = null;
        public Transform LD_Corner = null;
        public Transform RU_Corner = null;
        public Transform RD_Corner = null;
        [Header("Connectors")]
        public Transform U_Connector = null;
        public Transform L_Connector = null;
        public Transform D_Connector = null;
        public Transform R_Connector = null;

        public bool resizeContent = true;

        private const float LOCATION_MULTIPLYER = 0.5f;
        private const float SIZE_MULTIPLYER = 6.25f;

        public void CorrectSize()
        {
            CorrectFillForResize();
            CorrectFrameForResize();
            CorrectBcgForResize();
        }

        private void CorrectFillForResize()
        {
            var resizeValue = size;
            //Positions
            var locationX = -0.08f + resizeValue.x * LOCATION_MULTIPLYER;
            var locationY = -0.08f + resizeValue.y * LOCATION_MULTIPLYER;

            LU_Corner.localPosition = new Vector3(-locationX, 0, locationY);
            LD_Corner.localPosition = new Vector3(-locationX, 0, -locationY);
            RU_Corner.localPosition = new Vector3(locationX, 0, locationY);
            RD_Corner.localPosition = new Vector3(locationX, 0, -locationY);
            U_Connector.localPosition = new Vector3(0, 0, locationY);
            L_Connector.localPosition = new Vector3(locationX, 0, 0);
            D_Connector.localPosition = new Vector3(0, 0, -locationY);
            R_Connector.localPosition = new Vector3(-locationX, 0, 0);
            //Sizes
            var sizeX = -1.98f + resizeValue.x * SIZE_MULTIPLYER;
            var sizeY = -1.98f + resizeValue.y * SIZE_MULTIPLYER;

            R_Connector.localScale = new Vector3(1, 1, sizeY);
            U_Connector.localScale = new Vector3(sizeX, 1, 1);
            L_Connector.localScale = new Vector3(1, 1, sizeY);
            D_Connector.localScale = new Vector3(sizeX, 1, 1);
        }

        private void CorrectFrameForResize()
        {
            frameRenderer.size = size;
        }
        private void CorrectBcgForResize()
        {
            bcgRenderrer.size = size;
            bcgBoxColider.size = new Vector3(size.x, size.y, 0.05f);
            if(resizeContent)
                contentContener.localScale = new Vector3(Mathf.Min(size.x, size.y), 1, Mathf.Min(size.x, size.y));
        }
    }
}
