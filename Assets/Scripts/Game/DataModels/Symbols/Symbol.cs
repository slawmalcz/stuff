﻿using UnityEngine;

namespace Game.DataModels
{
    [CreateAssetMenu(menuName = "BoardElements/Symbols")]
    public class Symbol : ScriptableObject
    {
        public Texture albedoTransparencyMap = null;
        public Texture emissionMap = null;
    
        public void AssignSymbol (Material material)
        {
            material.EnableKeyword("_BaseMap");
            material.EnableKeyword("_EMISSION");
    
            material.SetTexture(Shader.PropertyToID("_BaseMap"), albedoTransparencyMap);
            material.SetTexture(Shader.PropertyToID("_EmissionMap"), emissionMap);
        }
    }
}
