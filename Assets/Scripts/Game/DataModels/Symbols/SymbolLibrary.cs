﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.DataModels
{
    public static class SymbolLibrary
    {
        private static List<Symbol> _symbols = null;
        public static List<Symbol> Symbols {
            get {
                if (_symbols is null || _symbols.Count == 0)
                    _symbols = new List<Symbol>(Resources.LoadAll<Symbol>("SymbolsData"));
                return _symbols;
            }
        }
    }
}
