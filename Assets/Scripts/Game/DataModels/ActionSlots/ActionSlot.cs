﻿using Game.DataModels.Recepies;
using Inspector;
using System;
using System.Linq;
using UnityEngine;

namespace Game.DataModels
{
    [CreateAssetMenu(menuName = "BoardElements/ActionSlot")]
    public class ActionSlot : ScriptableObject, ICloneable
    {
        [NonSerialized] private const string RESOURCE_PATH = "ActionSlots";
        public int id;

        public string actionName;
        public string header;
        [TextArea(5,10)] public string description;
        public Texture2D icon;
        public int lifeTime;

        public CardSocked [] cardSockeds;
        public Recepie[] possibleRecepies;

        public Recepie nullRecepie;

        public object Clone()
        {
            var output = ScriptableObject.CreateInstance<ActionSlot>();
            output.id = id;
            output.actionName = actionName;
            output.header = header;
            output.description = description;
            output.icon = icon;
            output.cardSockeds = (CardSocked[])cardSockeds.Clone();
            output.possibleRecepies = (Recepie[])possibleRecepies.Clone();
            output.lifeTime = lifeTime;
            output.nullRecepie = nullRecepie;
            return output;
        }

        #region Overrides
        public static bool operator ==(ActionSlot a, ActionSlot b)
        {
            {
                if (a is null && b is null)
                    return true;
                if (a is null || b is null)
                    return false;
                return a.Equals(b);
            }
        }
        public static bool operator !=(ActionSlot a, ActionSlot b)
        {
            if (a is null && b is null)
                return false;
            if (a is null || b is null)
                return true;
            return !a.Equals(b);
        }

        public override bool Equals(object other)
        {
            if (other is ActionSlot)
                return ((ActionSlot)other).id == id;
            return false;
        }

        public override int GetHashCode()
        {
            var hashCode = 1545243542;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + id.GetHashCode();
            return hashCode;
        }

        public override string ToString() => $"Slot:{actionName}";
        #endregion
    }
}
