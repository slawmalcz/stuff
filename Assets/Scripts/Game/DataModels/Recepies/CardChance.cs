﻿using System;
using UnityEngine;

namespace Game.DataModels.Recepies
{
    [Serializable]
    public struct CardChance
    {
        public Card card;
        [Range(0,1)] public float chance;
    }
}
