﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.DataModels.Recepies
{
    [Serializable]
    public class ActionSlotReward
    {
        public bool IsExclusive = false;
        public ActionSlotChance[] output;

        public List<ActionSlot> GetReward()
        {
            var ret = new List<ActionSlot>();
            if (IsExclusive)
            {
                var maxValue = output.ToList().Sum(x => x.chance);
                var randomOutput = UnityEngine.Random.Range(0, maxValue);
                for (var i = 0; i < output.Length; i++)
                {
                    randomOutput -= output[i].chance;
                    if (randomOutput < 0)
                    {
                        ret.Add(output[i].actionSlot);
                        break;
                    }
                }
            }
            else
            {
                var randomSequence = new System.Random(Time.frameCount);
                for (var i = 0; i < output.Length; i++)
                    if (randomSequence.NextDouble() < output[i].chance)
                        ret.Add(output[i].actionSlot);

            }
            return ret;
        }
    }
}
