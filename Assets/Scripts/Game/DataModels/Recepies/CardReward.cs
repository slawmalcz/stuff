﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.DataModels.Recepies
{
    [Serializable]
    public class CardReward
    {
        public bool IsExclusive = false;
        public CardChance[] output;

        public List<Card> GetReward()
        {
            var ret = new List<Card>();
            if (IsExclusive)
            {
                var maxValue = output.ToList().Sum(x => x.chance);
                var randomOutput = UnityEngine.Random.Range(0, maxValue);
                for (var i = 0; i < output.Length; i++)
                {
                    randomOutput -= output[i].chance;
                    if(randomOutput < 0)
                    {
                        ret.Add(output[i].card);
                        break;
                    }
                }
            }
            else
            {
                var randomSequence = new System.Random(Time.frameCount);
                for (var i = 0; i < output.Length; i++)
                {
                    var chance = randomSequence.NextDouble();
                    if (chance < output[i].chance)
                        ret.Add(output[i].card);
                }

            }
            return ret;
        }

    }
}
