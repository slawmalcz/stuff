﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.DataModels.Recepies
{
    [Serializable]
    public class CardAligmentElement
    {
        public Card card;
        public bool status;
    }

    [Serializable]
    public class SlotAligmentElement
    {
        public ActionSlot slot;
        public bool status;
    }

    [CreateAssetMenu(menuName = "BoardElements/Recepie")]
    public class Recepie : ScriptableObject
    {
        [NonSerialized] private const string RESOURCE_PATH = "Recepies";
        public int id;

        public CardAligmentElement[] requiredCardsOnHandToExecute;
        public SlotAligmentElement[] requiredSlotsOnHandToExecut;

        public Card[] requiredCards;

        public new string name;
        public string header;
        [TextArea(5, 10)] public string descriptions;

        public CardReward[] outputCards;
        public ActionSlotReward[] outputActionSlots;
        public int executionTime = 0;

        public bool isEnding = false;

        public bool CanBeAccesed(List<Card> cardUsed)
        {
            return requiredCards.Length == 0 || requiredCards.All(x => cardUsed.Contains(x));
        }

        public bool CanBeExecuted(List<Card> cardOnHand, List<ActionSlot> slotsOnHand)
        {
            var cardsRequred = requiredCardsOnHandToExecute.Where(x => x.status).ToList();
            var cardsRequredTask = (cardsRequred.Count() > 0)?cardsRequred.All(x => cardOnHand.Contains(x.card)):true;

            var cardsUndisired = requiredCardsOnHandToExecute.Where(x => !x.status).ToList();
            var cardsUndisiredTask = (cardsUndisired.Count() > 0)?cardsUndisired.All(x => !cardOnHand.Contains(x.card)):true;

            var slotRequred = requiredSlotsOnHandToExecut.Where(x => x.status).ToList();
            var slotRequredTask = (slotRequred.Count() > 0)?slotRequred.All(x => slotsOnHand.Contains(x.slot)):true;
                
            var slotUndisired = requiredSlotsOnHandToExecut.Where(x => !x.status).ToList();
            var slotUndisiredTask = (slotUndisired.Count() > 0)?slotUndisired.All(x => !slotsOnHand.Contains(x.slot)):true;

            return cardsRequredTask && cardsUndisiredTask && slotRequredTask && slotUndisiredTask;
        }

        public object Clone()
        {
            var output = ScriptableObject.CreateInstance<Recepie>();
            output.id = id;
            output.requiredCardsOnHandToExecute = (CardAligmentElement[])requiredCardsOnHandToExecute.Clone();
            output.requiredSlotsOnHandToExecut = (SlotAligmentElement[])requiredSlotsOnHandToExecut.Clone();
            output.requiredCards = (Card[])requiredCards.Clone();
            output.name = name;
            output.header = header;
            output.descriptions = descriptions;
            output.outputCards = (CardReward[])outputCards.Clone();
            output.outputActionSlots = (ActionSlotReward[])outputActionSlots.Clone();
            output.executionTime = executionTime;
            output.isEnding = isEnding;
            return output;
    }

        public Card[] GetCardReward()
        {
            var ret = new List<Card>();
            foreach (var element in outputCards)
                ret.AddRange(element.GetReward());
            return ret.ToArray();
        }

        public ActionSlot[] GetActionSlotReward()
        {
            var ret = new List<ActionSlot>();
            foreach (var element in outputActionSlots)
                ret.AddRange(element.GetReward());
            return ret.ToArray();
        }

        #region Overrides
        public static bool operator ==(Recepie a, Recepie b)
        {
            {
                if (a is null && b is null)
                    return true;
                if (a is null || b is null)
                    return false;
                return a.Equals(b);
            }
        }
        public static bool operator !=(Recepie a, Recepie b)
        {
            if (a is null && b is null)
                return false;
            if (a is null || b is null)
                return true;
            return !a.Equals(b);
        }

        public override bool Equals(object other)
        {
            if (other is Recepie)
                return ((Recepie)other).id == id;
            return false;
        }

        public override int GetHashCode()
        {
            var hashCode = 1545243542;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + id.GetHashCode();
            return hashCode;
        }

        public override string ToString() => $"Recepie:{name}";
        #endregion
    }
}
