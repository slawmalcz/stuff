﻿using System;
using UnityEngine;

namespace Game.DataModels.Recepies
{
    [Serializable]
    public struct ActionSlotChance
    {
        public ActionSlot actionSlot;
        [Range(0, 1)] public float chance;
    }
}
