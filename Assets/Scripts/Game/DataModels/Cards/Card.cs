﻿using Inspector;
using System;
using System.Linq;
using UnityEngine;

namespace Game.DataModels
{
    [CreateAssetMenu(menuName = "BoardElements/Card")]
    public class Card: ScriptableObject
    {
        public int id;

        public new string name;
        public CardsType type;
        public Sprite icon;

        #region Overrides
        public static bool operator ==(Card a, Card b)
        {
            {
                if (a is null && b is null)
                    return true;
                if (a is null || b is null)
                    return false;
                return a.Equals(b);
            }
        }
        public static bool operator !=(Card a, Card b)
        {
            if (a is null && b is null)
                return false;
            if (a is null || b is null)
                return true;
            return !a.Equals(b);
        }

        public override bool Equals(object other)
        {
            if (other is Card)
                return ((Card)other).id == id;
            return false;
        }

        public override int GetHashCode()
        {
            var hashCode = 1545243542;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + id.GetHashCode();
            return hashCode;
        }

        public override string ToString() => $"Card:{name}";
        #endregion
    }
}
