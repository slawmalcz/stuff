﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.DataModels
{
    public static class CardTypeLibrary
    {
        private const string CARD_TYPES_PATH = "";
        private static List<CardsType> _types = null;

        public static List<CardsType> Types {
            get {
                if (_types is null)
                    _types = GetCardsTypes();
                return _types;
            }
        }

        public static List<CardsType> GetCardTypes(Card card) => Types.Where(x => x.extendedCards.Contains(card)).ToList();

        private static List<CardsType> GetCardsTypes() => Resources.LoadAll<CardsType>(CARD_TYPES_PATH).ToList();
    }
}
