﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.DataModels
{
    [CreateAssetMenu(menuName = "BoardElements/CardsType")]
    public class CardsType : ScriptableObject
    {
        public string groupName;
        public Texture2D groupIcon;
        public List<Card> extendedCards;
    }
}
