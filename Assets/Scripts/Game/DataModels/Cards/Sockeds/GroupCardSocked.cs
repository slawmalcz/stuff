﻿using System.Linq;
using UnityEngine;

namespace Game.DataModels
{
    [CreateAssetMenu(menuName = "BoardElements/Cards/GroupCardSlot")]
    public class GroupCardSocked : CardSocked
    {
        public CardsType[] allowedTypes;

        public override bool CanBePlaced(Card card) => allowedTypes.Any(x => CardTypeLibrary.GetCardTypes(card).Contains(x));
    }
}
