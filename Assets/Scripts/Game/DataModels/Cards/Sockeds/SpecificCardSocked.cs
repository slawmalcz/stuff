﻿using System.Linq;
using UnityEngine;

namespace Game.DataModels
{
    [CreateAssetMenu(menuName = "BoardElements/Cards/SpecificCardSlot")]
    public class SpecificCardSocked : CardSocked
    {
        public Card[] allowedCards;

        public override bool CanBePlaced(Card card) => allowedCards.ToList().Any(x => x == card);
    }
}
