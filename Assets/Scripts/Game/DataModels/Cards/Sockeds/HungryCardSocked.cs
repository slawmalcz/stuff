﻿using System.Linq;
using UnityEngine;

namespace Game.DataModels
{
    [CreateAssetMenu(menuName = "BoardElements/Cards/HungryCardSlot")]
    public class HungryCardSocked : CardSocked
    {
        public Card[] requiredCards;

        public override bool CanBePlaced(Card card) => requiredCards.Contains(card);
    }
}
