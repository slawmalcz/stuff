﻿namespace Game.DataModels
{
    public enum CardSocketState
    {
        CardSlot,
        SealdCardSlot,
        Empty,
        Back,
        SealdBack,
        BackEmpty
    }
}
