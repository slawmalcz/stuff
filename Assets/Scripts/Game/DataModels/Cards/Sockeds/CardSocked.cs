﻿using Game.Controllers;
using UnityEngine;

namespace Game.DataModels
{
    public abstract class CardSocked : ScriptableObject
    {
        public CardSocketState currentState = CardSocketState.Empty;

        public virtual bool CanBePlaced(CardController cardController)
        {
            if (cardController is null)
                return false;
            return CanBePlaced(cardController.cardData);
        }
        public abstract bool CanBePlaced(Card card);
    }
}
