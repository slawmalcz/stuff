﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Menu
{
    [Serializable]
    public class SaveFile
    {
        public int slot;
        public string name;
        public int turn;
        public ActionSlotData[] actionSlotDatas;
        public int[] cardsId;
        public Vector3[] cardsPositions;
        public int[] schedulerTurns;
        public int[] schedulersRecepies;

        public List<(int, Vector3, int[], int)> ActionSlotsData {
            get {
                var ret = new List<(int, Vector3, int[], int)>();
                for (var i = 0; i < actionSlotDatas.Length; i++)
                    ret.Add((actionSlotDatas[i].id, actionSlotDatas[i].position, actionSlotDatas[i].cards, actionSlotDatas[i].age));
                return ret;
            }
        }

        public List<(int, Vector3)> CardData {
            get {
                var ret = new List<(int, Vector3)>();
                for (var i = 0; i < cardsId.Length; i++)
                    ret.Add((cardsId[i], cardsPositions[i]));
                return ret;
            }
        }

        public List<(int,int)> Schedulers {
            get {
                var ret = new List<(int, int)>();
                for (var i = 0; i < schedulerTurns.Length; i++)
                    ret.Add((schedulerTurns[i], schedulersRecepies[i]));
                return ret;
            }
        }

        #region Constructors
        public SaveFile(int slotNumber, string name, int turn, (int, Vector3, int[], int)[] actionSlotToSerialize, (int, Vector3)[] cardToSerialize, (int, int)[] scheduler) :
            this(slotNumber, name, turn,
                actionSlotToSerialize.Select(x => new ActionSlotData(x.Item1, x.Item2, x.Item3, x.Item4)).ToArray(),
                cardToSerialize.Select(x => x.Item1).ToArray(),
                cardToSerialize.Select(x => x.Item2).ToArray(),
                scheduler.Select(x => x.Item1).ToArray(),
                scheduler.Select(x => x.Item1).ToArray()
                )
        { }

        public SaveFile(int slotNumber, string name, int turn, List<(int, Vector3, int[], int)> actionSlotToSerialize, List<(int, Vector3)> cardToSerialize,List<(int,int)> scheduler)
            : this(slotNumber, name, turn, actionSlotToSerialize.ToArray(), cardToSerialize.ToArray(), scheduler.ToArray()) { }

        private SaveFile(int slot, string name, int turn, ActionSlotData[] actionSlotDatas, int[] cardsId, Vector3[] cardsPositions,int[] schedulerTurns,int[] schedulersRecepies)
        {
            this.slot = slot;
            this.name = name;
            this.turn = turn;
            this.actionSlotDatas = actionSlotDatas;
            this.cardsId = cardsId;
            this.cardsPositions = cardsPositions;
            this.schedulerTurns = schedulerTurns;
            this.schedulersRecepies = schedulersRecepies;
        }
        #endregion
    }
}
