﻿using System;
using UnityEngine;

namespace Menu
{
    [Serializable]
    public class ActionSlotData
    {
        public int id;
        public Vector3 position;
        public int[] cards;
        public int age;

        public ActionSlotData(int id, Vector3 position, int[] cards,int age)
        {
            this.id = id;
            this.position = position;
            this.cards = cards;
            this.age = age;
        }
    }
}
