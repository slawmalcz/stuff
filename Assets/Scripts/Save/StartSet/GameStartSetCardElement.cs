﻿using Game.DataModels;
using System;
using UnityEngine;

namespace Save.StartSet
{
    [Serializable]
    public class GameStartSetCardElement
    {
        public Card card;
        public Vector3 position;
    }
}
