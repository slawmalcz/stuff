﻿using System.Collections.Generic;
using UnityEngine;

namespace Save.StartSet
{
    [CreateAssetMenu(menuName = "MainMenu/GameStartSet")]
    public class GameStartSet : ScriptableObject
    {
        public GameStartSetActionSlotElement[] startingActionSlots;
        public GameStartSetCardElement[] startingCards;
        public GameStartSetSchedulerElement[] scheduler;

        public List<(int,Vector3)> ActionSlots {
            get {
                var toTransferActionSlots = new List<(int, Vector3)>();
                foreach (var line in startingActionSlots)
                    toTransferActionSlots.Add((line.actionSlot.id, line.position));
                return toTransferActionSlots;
            }
        }

        public List<(int, Vector3)> Cards {
            get {
                var toTransferCards = new List<(int, Vector3)>();
                foreach (var line in startingCards)
                    toTransferCards.Add((line.card.id, line.position));
                return toTransferCards;
            }
        }

        public List<(int, int)> Scheduler {
            get {
                var ret = new List<(int, int)>();
                if (scheduler is null)
                    return ret;
                foreach (var line in scheduler)
                    ret.Add((line.turToCall, line.recepie.id));
                return ret;
            }
        }
    }
}
