﻿using Game.DataModels.Recepies;
using System;

namespace Save.StartSet
{
    [Serializable]
    public class GameStartSetSchedulerElement
    {
        public int turToCall;
        public Recepie recepie;
    }
}
