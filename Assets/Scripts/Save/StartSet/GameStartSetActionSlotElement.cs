﻿using Game.DataModels;
using System;
using UnityEngine;

namespace Save.StartSet
{
    [Serializable]
    public class GameStartSetActionSlotElement
    {
        public ActionSlot actionSlot;
        public Vector3 position;
    }
}
