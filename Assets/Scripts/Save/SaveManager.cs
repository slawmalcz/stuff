﻿using Menu;
using Save.StartSet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Save
{
    public class SaveManager
    {
        #region SingletonImpelementation
        private static SaveManager _instance = null;
        public static SaveManager Instance {
            get {
                if (_instance is null)
                    _instance = new SaveManager();
                return _instance;
            }
        }
        #endregion

        private string SaveFileDataPath => $"{Application.dataPath}/Save";
        private const string SAVE_EXTENSIONS = ".save";
        private const string NULL_DATA_STRING = "=:=:=";
        private const string RESOURCE_START_SET_PATH = "Game/GemeStartSets";

        private Dictionary<int, string> currentSaves = null;
        public Dictionary<int, string> CurretnSaves {
            get {
                if (currentSaves is null)
                    ReloadSaveFiles();
                return currentSaves;
            }
        }

        public SaveManager() => ReloadSaveFiles();

        private void ReloadSaveFiles()
        {
            currentSaves = new Dictionary<int, string>();
            var files = Directory.GetFiles(SaveFileDataPath).Where(x => Path.GetExtension(x) == SAVE_EXTENSIONS);
            foreach (var line in files)
            {
                var saveFile = ReadFromFile(line);
                if (!(saveFile is null) && !currentSaves.ContainsKey(saveFile.slot))
                    currentSaves.Add(saveFile.slot,Path.GetFileName(line));
            }
        }

        #region DataGetters
        public SaveFile ExtractSaveFile (int saveSlotNumber)
        {
            if (currentSaves.ContainsKey(saveSlotNumber))
                return ReadFromFile($"{Application.dataPath}/Save/{currentSaves[saveSlotNumber]}");
            else
                return null;
        }

        public (string,string) ExtractSaveFileInfo (int saveSlotnumber)
        {
            if (currentSaves.ContainsKey(saveSlotnumber))
            {
                var saveFile = ExtractSaveFile(saveSlotnumber);
                var readedDate = File.GetLastWriteTime($"{Application.dataPath}/Save/{saveFile.name}{SAVE_EXTENSIONS}").ToString(System.Globalization.CultureInfo.InvariantCulture);
                return (saveFile.name, readedDate);
            }
            else
                return (NULL_DATA_STRING, NULL_DATA_STRING);
        }
        public SaveFile GetNewestSaveFile()
        {
            if (CurretnSaves.Count > 0)
            {
                var toLoad = CurretnSaves.OrderBy(x => File.GetLastWriteTime($"{Application.dataPath}/Save/{x.Value}{SAVE_EXTENSIONS}")).ToArray()[0].Key;
                return Instance.ExtractSaveFile(toLoad);
            }
            return null;
        }
        #endregion

        #region SaveFileCreation
        private SaveFile CreateEmptySaveFile(int slotNumber, string name, GameStartSet startSet)
        {
            var list = new List<(int, Vector3, int[],int)>();
            for (var i = 0; i < startSet.ActionSlots.Count; i++)
                list.Add((startSet.ActionSlots[i].Item1, startSet.ActionSlots[i].Item2, new int[0],0));
            return new SaveFile(slotNumber, name, 0, list, startSet.Cards, startSet.Scheduler);
        }

        public SaveFile CreteNewSave(string saveSlotName, int sloatNumber)
        {
            //Deleting existing save if needed
            if (!(ExtractSaveFile(sloatNumber) is null))
                DeleteFile(CurretnSaves[sloatNumber]);
            //Craeate save
            var listOfStarts = Resources.LoadAll<GameStartSet>(RESOURCE_START_SET_PATH);
            var selectedIndex = UnityEngine.Random.Range(0, listOfStarts.Length - 1);
            var createdSaveSlot = CreateEmptySaveFile(sloatNumber, saveSlotName, listOfStarts[selectedIndex]);
            WriteToFile(createdSaveSlot);
            return createdSaveSlot;
        }
        #endregion

        #region FileManipulation
        public void SaveCurrentSaveFile(SaveFile saveFile) => WriteToFile(saveFile);

        private void WriteToFile(SaveFile save)
        {
            var json = JsonUtility.ToJson(save);
            string password = GeneratePassword();
            var outputString = string.Concat(new string[] { password, /*encryptedMessage*/ StringCipher.Encrypt(json, password) });
            var newFilePath = $"{Application.dataPath}/Save/{save.name}{SAVE_EXTENSIONS}";
            var newFile = File.Create(newFilePath);
            byte[] info = new UTF8Encoding(true).GetBytes(outputString);
            newFile.Write(info, 0, info.Length);
            newFile.Close();
            File.SetCreationTime(newFilePath, DateTime.Now);
            AssetDatabase.Refresh();
            ReloadSaveFiles();
        }

        private SaveFile ReadFromFile(string path)
        {
            var readedString = System.IO.File.ReadAllText(path);
            string password = readedString.Substring(0, PASSWORD_LENGTH);
            readedString = readedString.Substring(PASSWORD_LENGTH, readedString.Length - PASSWORD_LENGTH);
            string decryptedstring = StringCipher.Decrypt(readedString, password);
            var ret = JsonUtility.FromJson<SaveFile>(decryptedstring);
            return ret;
        }

        private void DeleteFile(string path)
        {
            if (File.Exists($"{Application.dataPath}/Save/{path}"))
            {
                File.Delete($"{Application.dataPath}/Save/{path}");
                File.Delete($"{Application.dataPath}/Save/{path}.meta");

            }
        }
        #endregion

        #region PasswordGeneration
        private const int PASSWORD_LENGTH = 16;

        private string GeneratePassword()
        {
            var randomInstance = new System.Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, PASSWORD_LENGTH)
              .Select(s => s[randomInstance.Next(s.Length)]).ToArray());
        }
        #endregion
    }
}
