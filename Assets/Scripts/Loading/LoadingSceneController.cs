﻿using Game.Controllers;
using Menu;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utilities;
using Utilities.TaskSchedulers;

namespace Assets.Scripts.Loading
{
    class LoadingSceneController : MonoBehaviour
    {
        private SaveFile loadedSave = null;
        private bool isReady = false;
        public Slider uiSlider;

        private float resouceLoadProgress = 0;
        private AsyncOperation sceneLoadingOperation = null;

        public List<GameObject> destroyOnUnload;

        internal void Init(SaveFile save)
        {
            loadedSave = save;
            resourceTask = new Task(LoadResources());
            resourceTask.Pause();
            loadSceneTask = new Task(LoadScene());
            loadSceneTask.Pause();
            onSceneLoadedTask = new Task(OnLoadScene());
            onSceneLoadedTask.Pause();

            destroyOnUnload = SceneManager.GetActiveScene().GetRootGameObjects().ToList();
            if(destroyOnUnload.Contains(gameObject))
                destroyOnUnload.Remove(gameObject);
            isReady = true;

        }

        private Task resourceTask;
        private Task loadSceneTask;
        private Task onSceneLoadedTask;
        private int stage = 0;

        public void Update()
        {
            if (!isReady)
                return;
            
            switch (stage)
            {
                case (0):
                    resourceTask.Unpause();
                    stage = 1;
                    break;
                case (1):
                    if (!resourceTask.Running)
                        stage = 2;
                    break;
                case (2):
                    loadSceneTask.Unpause();
                    stage = 3;
                    break;
                case (3):
                    if (!loadSceneTask.Running)
                        stage = 4;
                    break;
                case (4):
                    onSceneLoadedTask.Unpause();
                    stage = 5;
                    break;
                case (5):
                    if (!onSceneLoadedTask.Running)
                        stage = 6;
                    break;
                default:
                    break;
            }

            var listOfTasksProgresses = new List<float>(
                new float[] {
                    resouceLoadProgress,
                    (sceneLoadingOperation is null)?0:sceneLoadingOperation.progress,
                }
            );
            var sumValue = listOfTasksProgresses.Average();
            uiSlider.value = sumValue;               
        }

        private IEnumerator LoadResources()
        {
            ResourceManager.Initialize((float x) => resouceLoadProgress = x);
            yield return new WaitUntil(() => ResourceManager.IsReady);
        }
        private IEnumerator LoadScene()
        {
            sceneLoadingOperation = SceneManager.LoadSceneAsync("GameScene", LoadSceneMode.Additive);
            yield return new WaitUntil(() => sceneLoadingOperation.isDone);
            sceneLoadingOperation.allowSceneActivation = false;
        }

        private IEnumerator OnLoadScene()
        {
            sceneLoadingOperation.allowSceneActivation = true;
            yield return new WaitForEndOfFrame();
            GameController.LoadGameFile(loadedSave);
            yield return new WaitForEndOfFrame();
            Destroy(gameObject);
            _ = SceneManager.UnloadSceneAsync("LoadingScene");
        }
    }
}
