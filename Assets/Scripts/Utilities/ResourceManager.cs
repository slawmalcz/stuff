﻿using Game.DataModels;
using Game.DataModels.Recepies;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Utilities
{
    public abstract class DataLoader
    {
        public float Progres => currentReadyElements / maxElements;
        public bool IsReady => Progres == 1f;

        protected readonly int maxElements;
        protected readonly List<string> directoriesToCheck;
        protected int currentReadyElements;

        

        protected DataLoader(string dataFolderName,string pathOfResourcesStories)
        {
            directoriesToCheck = Directory.GetDirectories(pathOfResourcesStories).Select(x => Path.Combine(x, dataFolderName)).ToList();
            maxElements = directoriesToCheck.Count();
            currentReadyElements = 0;
        }

    }
    public class DataLoader<T> : DataLoader where T: ScriptableObject
    {
        private string pathOfResources;
        public List<T> Resoult;

        public DataLoader(string dataFolderName,string pathOfResourcesStories, string pathOfResources) : base(dataFolderName, pathOfResourcesStories) 
        {
            this.pathOfResources = pathOfResources;
        }

        public DataLoader Start() => GetData();
        private DataLoader GetData()
        {
            var ret = new List<T>();
            foreach (var element in directoriesToCheck.Select(x => GetCardsFromDirectory(x)).ToList())
                ret.AddRange(element);
            Resoult = ret;
            return this;
        }

        private List<T> GetCardsFromDirectory(string path)
        {
            if (!Directory.Exists(path))
                return new List<T>();
            return Resources.LoadAll<T>(path.Substring(pathOfResources.Length + 1)).ToList();
        }

    }

    public class ResourceManager 
    {
        public static bool IsReady { get; private set; } = false;

        internal static void Initialize(Action<float> assignProgress)
        {
            if (Instance is null)
            {
                Instance = new ResourceManager();
                Instance.LoadData(assignProgress);
                IsReady = true;
            }
        }

        private static ResourceManager Instance = null;

        public static Card GetCardById(int id) => (IsReady && Instance.cards.ContainsKey(id)) ? Instance.cards[id] : null;
        public static ActionSlot GetActionSlotById(int id) => (IsReady && Instance.actionSlots.ContainsKey(id)) ? (ActionSlot)Instance.actionSlots[id].Clone() : null;
        public static Recepie GetRecepieById(int id) => (IsReady && Instance.recepies.ContainsKey(id)) ? (Recepie)Instance.recepies[id].Clone() : null;

        private Dictionary<int, Card> cards;
        private Dictionary<int, ActionSlot> actionSlots;
        private Dictionary<int, Recepie> recepies;

        private const string CardsFolderName = "Cards";
        private const string ActionSloatFolderName = "ActionSlots";
        private const string RecepiesFolderName = "Recepies";

        private readonly DataLoader<Card> cardLoader;
        private readonly DataLoader<ActionSlot> actionSloatLoader;
        private readonly DataLoader<Recepie> recepieLoader;

        public ResourceManager()
        {
            var pathOfResources = Path.Combine(Application.dataPath, "Resources");
            var pathOfResourcesStories = Path.Combine(Application.dataPath, "Resources/Stories");

            cardLoader = new DataLoader<Card>(CardsFolderName, pathOfResourcesStories, pathOfResources);
            actionSloatLoader = new DataLoader<ActionSlot>(ActionSloatFolderName, pathOfResourcesStories, pathOfResources);
            recepieLoader = new DataLoader<Recepie>(RecepiesFolderName, pathOfResourcesStories, pathOfResources);
        }

        private void LoadData(Action<float> assignProgress)
        {
            cardLoader.Start();
            cards = cardLoader.Resoult.ToDictionary(x=>x.id);
            assignProgress.Invoke(0.33f);
            actionSloatLoader.Start();
            actionSlots = actionSloatLoader.Resoult.ToDictionary(x => x.id);
            assignProgress.Invoke(0.66f);
            recepieLoader.Start();
            recepies = recepieLoader.Resoult.ToDictionary(x => x.id);
            assignProgress.Invoke(1f);
        }
    }

}
