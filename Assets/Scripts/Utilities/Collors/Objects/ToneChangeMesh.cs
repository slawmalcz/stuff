﻿using UnityEngine;

namespace Utilities.Colors
{
    public class ToneChangeMesh : AbstractToneElement
    {
        public Material meshMaterial = null;
        protected override void AssignColor(Color color) => meshMaterial.color = color;
    }
}
