﻿using UnityEngine;

namespace Utilities.Colors
{
    public abstract class AbstractToneElement : MonoBehaviour
    {
        private ColorPalletsController _currentController = null;
        public ColorPalletsController CurrentController {
            get {
                if (_currentController is null)
                    _currentController = GameObject.FindObjectOfType<ColorPalletsController>();
                return _currentController;
            }
        }

        public virtual CollorPanetOptions CollorOption {
             get => CollorPanetOptions.MainTone;
        }

        protected abstract void AssignColor(Color color);
        public void AssignColor(int tone) => AssignColor(CurrentController.GetMixedColors().GetColor(CollorOption, tone));

        public void Awake() => AssignColor(CurrentController.GetMixedColors().GetColor(CollorOption));
    }
}
