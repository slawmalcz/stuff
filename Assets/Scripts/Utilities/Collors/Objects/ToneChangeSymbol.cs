﻿using Game.DataModels;
using UnityEngine;

namespace Utilities.Colors
{
    public class ToneChangeSymbol : AbstractToneElement
    {
        [HideInInspector]
        public override CollorPanetOptions CollorOption => CollorPanetOptions.Symbols;
        public bool isRandom = true;
        private Material symbolMaterial = null;

        public new void Awake()
        {
            if (isRandom)
                Init(SymbolLibrary.Symbols[Mathf.FloorToInt(Random.value * SymbolLibrary.Symbols.Count)]);
            base.Awake();
        }

        public void Init(Symbol symbol)
        {
            symbolMaterial = Instantiate(GetComponent<MeshRenderer>().material, transform);
            GetComponent<MeshRenderer>().material = symbolMaterial;
            if (symbol != null)
            {
                symbol.AssignSymbol(symbolMaterial);
                AssignColor(CurrentController.GetMixedColors().GetColor(CollorOption, 0));
            }
        }

        protected override void AssignColor(Color color) => symbolMaterial.SetColor(Shader.PropertyToID("_EmissionColor"), color);
    }
}
