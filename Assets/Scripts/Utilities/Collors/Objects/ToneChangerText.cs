﻿using TMPro;
using UnityEngine;

namespace Utilities.Colors
{
    public class ToneChangerText : AbstractToneElement
    {
        public TextMeshPro element;

        protected override void AssignColor(Color color) => element.faceColor = color;
    }
}
