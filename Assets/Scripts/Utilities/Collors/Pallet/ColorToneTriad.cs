﻿using System;
using UnityEngine;

namespace Utilities.Colors
{
    [Serializable]
    public class ColorToneTriad
    {
        public CollorPanetOptions Signature;
        [ColorUsage(true,true)] public Color darkTone;
        [ColorUsage(true, true)] public Color midTone;
        [ColorUsage(true, true)] public Color brightTone;

        public ColorToneTriad(CollorPanetOptions signature, Color darkTone, Color midTone, Color brightTone)
        {
            Signature = signature;
            this.darkTone = darkTone;
            this.midTone = midTone;
            this.brightTone = brightTone;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0066:Convert switch statement to expression", Justification = "Not Implemented in Unity")]
        public Color GetTone(int tone)
        {
            switch (tone)
            {
                case 0:
                    return darkTone;
                case 1:
                    return midTone;
                case 2:
                    return brightTone;
                default:
                    return Color.magenta;
            }
        }

        public ColorToneTriad Clone() => new ColorToneTriad(Signature, darkTone, midTone, brightTone);

        public void Blend(Color darkTone, Color midTone, Color brightTone,float t)
        {
            this.darkTone = Color32.Lerp(this.darkTone,darkTone,t);
            this.midTone = Color32.Lerp(this.midTone, midTone, t); ;
            this.brightTone = Color32.Lerp(this.brightTone, brightTone, t); ;
        }
        public void Blend(ColorToneTriad triade, float t)
        {
            darkTone = Color32.Lerp(darkTone, triade.darkTone, t);
            midTone = Color32.Lerp(midTone, triade.midTone, t); ;
            brightTone = Color32.Lerp(brightTone, triade.brightTone, t); ;
        }
    }
}
