﻿using System.Linq;
using UnityEngine;

namespace Utilities.Colors
{

    [CreateAssetMenu(menuName = "Ui/ColorPallet")]
    public class ColorPallet : ScriptableObject
    {
        public ColorToneTriad[] colorTriads;

        public ColorPallet(ColorToneTriad[] colorTriads) => this.colorTriads = colorTriads;

        //[HideInInspector]
        public float Weight = 0;

        /// <summary>
        /// Give apropriet Color:
        /// [0] - Dark , [1] - Mid , [2] - Bright
        /// </summary>
        /// <param name="setting">Type of collor</param>
        /// <param name="tone"> </param>
        /// <returns>Color</returns>
        public Color GetColor(CollorPanetOptions setting,int tone = 1)
        {
            if (colorTriads.Any(x => x.Signature == setting))
                return colorTriads.First(x => x.Signature == setting).GetTone(tone);
            else
                return Color.magenta;
        }

        public ColorPallet Clone()
        {
            var ret = ScriptableObject.CreateInstance<ColorPallet>();
            ret.colorTriads = colorTriads.Select(x => x.Clone()).ToArray();
            ret.Weight = Weight;
            return ret;
        }

        public void Blend(ColorPallet colorPallet,float t)
        {
            for(var i = 0; i < colorTriads.Length; i++)
            {
                colorTriads[i].Blend(colorPallet.colorTriads[i], t);
            }
        }
    }
}
