﻿using System;
using UnityEngine;

namespace Utilities.Colors
{
    [Serializable]
    public class ColorPalletsController : MonoBehaviour
    {
        public ColorPallet[] colorPallets;
        [Tooltip("Data in minutes")]
        public float rateOfColorChange = 1;

        private ColorPallet selectedPallet = null;
        private float lastTimeSinceCheck = 0f;

        public ColorPallet GetMixedColors()
        {
            if (selectedPallet is null || lastTimeSinceCheck + rateOfColorChange * 60 < Time.unscaledTime)
            {
                lastTimeSinceCheck = Time.unscaledTime;
                selectedPallet = colorPallets[0].Clone();
                selectedPallet.name = "MaingGame Pallet Theme";
                for (var i = 1; i < colorPallets.Length; i++)
                    selectedPallet.Blend(colorPallets[i], colorPallets[i].Weight);
            }
            return selectedPallet;
        }
    }
}
