﻿using UnityEngine;
using System.Collections;

namespace Utilities.TaskSchedulers
{
    internal class TaskManager : MonoBehaviour
    {
        internal static TaskManager Instance;

        public static TaskState CreateTask(IEnumerator coroutine)
        {
            if (Instance == null)
            {
                var go = new GameObject("TaskManager");
                Instance = go.AddComponent<TaskManager>();
            }
            return new TaskState(coroutine);
        }
    }
}