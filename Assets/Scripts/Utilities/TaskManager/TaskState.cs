﻿using System.Collections;

namespace Utilities.TaskSchedulers
{
    internal class TaskState
    {
        public bool Running => running;
        public bool Paused => paused;

        public delegate void FinishedHandler(bool manual);
        public event FinishedHandler Finished;

        private readonly IEnumerator coroutine;
        private bool running;
        private bool paused;
        private bool stopped;

        public TaskState(IEnumerator c) => coroutine = c;

        public void Pause() => paused = true;

        public void Unpause() => paused = false;

        public void Start()
        {
            running = true;
            TaskManager.Instance.StartCoroutine(CallWrapper());
        }

        public void Stop()
        {
            stopped = true;
            running = false;
        }

        IEnumerator CallWrapper()
        {
            yield return null;
            var e = coroutine;
            while (running)
            {
                if (paused)
                    yield return null;
                else if (e != null && e.MoveNext())
                    yield return e.Current;
                else
                    running = false;
            }
            Finished?.Invoke(stopped);
        }
    }
}