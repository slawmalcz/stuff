﻿using System.Collections;

namespace Utilities.TaskSchedulers
{
    public class Task
	{
		private readonly TaskState task;

        public bool Running => task.Running;
        public bool Paused => task.Paused;

        public delegate void FinishedHandler(bool manual);
		public event FinishedHandler Finished;

		public Task(IEnumerator c, bool autoStart = true)
		{
			task = TaskManager.CreateTask(c);
			task.Finished += TaskFinished;
			if (autoStart)
				Start();
		}

        public void Start() => task.Start();
        public void Stop() => task.Stop();
        public void Pause() => task.Pause();
        public void Unpause() => task.Unpause();
        void TaskFinished(bool manual) => Finished?.Invoke(manual);
    }
}