﻿using Game.Controllers.Utility;
using UnityEditor;
using UnityEngine;

namespace Inspector
{
    [CustomEditor(typeof(FillSizeController))]
    public class FillSizeControllerEditor : Editor
    {
        private FillSizeController instance = null;

        void OnEnable()
        {
            instance = (FillSizeController)target;
            instance.CorrectSize();
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Fix"))
                instance.CorrectSize();
        }
    }
}
