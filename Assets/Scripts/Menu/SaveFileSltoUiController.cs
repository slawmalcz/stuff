﻿using System;
using TMPro;
using UnityEngine;

namespace Menu
{
    public class SaveFileSltoUiController : MonoBehaviour
    {
        [Header("Info")]
        private int saveFileSloatNumber;
        [Header("Components")]
        public TextMeshPro saveName;
        public TextMeshPro saveTime;

        private Action<int> loadSaveFunc = null;

        public void Init(int sloatNumber, string name, string saveTime, Action<int> loadSaveFunc)
        {
            saveName.text = name;
            this.saveTime.text = saveTime;
            this.loadSaveFunc = loadSaveFunc;
            saveFileSloatNumber = sloatNumber;
        }

        public void Init(int sloatNumber, (string, string) nameAndTime, Action<int> loadSaveFunc) =>
            Init(sloatNumber, nameAndTime.Item1, nameAndTime.Item2, loadSaveFunc);

        // On click action
        public void LoadSelectedSlot()
        {
            if (!(loadSaveFunc is null))
                loadSaveFunc.Invoke(saveFileSloatNumber);
        }
    }
}
