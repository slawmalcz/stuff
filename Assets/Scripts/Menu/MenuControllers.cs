﻿using Save;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class MenuControllers : MonoBehaviour
    {
        [Header("MenuTabs")]
        public GameObject newGameTab = null;
        public GameObject loadTab = null;
        public GameObject settingsTab = null;
        public GameObject creditsTab = null;
        public GameObject mainPanel = null;
        public GameObject newGameNamingTab = null;
        [Header("MainPadButtons")]
        public GameObject continueButton = null;
        [Header("Prefabs")]
        public GameObject savePasser = null;

        public void Start() => RestoreMainPanel();

        #region OnClickActions
        public void OnStartClick()
        {
            newGameTab.SetActive(true);
            mainPanel.SetActive(false);
            var newGameSlots = newGameTab.transform.Find("Content").Find("SaveSlots").GetComponentsInChildren<SaveFileSltoUiController>();
            for (var i = 0; i < newGameSlots.Length; i++)
                newGameSlots[i].GetComponent<SaveFileSltoUiController>()
                    .Init(i, SaveManager.Instance.ExtractSaveFileInfo(i),
                          (x) =>
                          {
                              CallNewGameNamingTab(x);
                              //Load(SaveManager.Instance.CreteNewSave($"TestFile{x}", x));
                          }
                     );
        }

        public void OnContinueClick() => Load(SaveManager.Instance.GetNewestSaveFile());

        public void OnLoadClick()
        {
            mainPanel.SetActive(false);
            loadTab.SetActive(true);
            var listOfSlots = loadTab.transform.Find("Content").Find("SaveSlots").GetComponentsInChildren<SaveFileSltoUiController>();
            for (var i = 0; i < listOfSlots.Length; i++)
                listOfSlots[i].GetComponent<SaveFileSltoUiController>().Init(i, SaveManager.Instance.ExtractSaveFileInfo(i), Load);
        }

        public void OnSettingsClick()
        {
            //TODO: Write settings for game
            mainPanel.SetActive(false);
            settingsTab.SetActive(true);
        }

        public void OnCreditsClicked()
        {
            //TODO: Write credits for game
            mainPanel.SetActive(false);
            creditsTab.SetActive(true);
        }

        public void OnExitClicked()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
        #endregion

        public void RestoreMainPanel()
        {
            mainPanel.SetActive(true);
            loadTab.SetActive(false);
            settingsTab.SetActive(false);
            newGameTab.SetActive(false);
            creditsTab.SetActive(false);
            newGameNamingTab.SetActive(false);
            if (SaveManager.Instance.CurretnSaves.Count > 0)
                continueButton.SetActive(true);
        }

        public void CallNewGameNamingTab(int selectedSlot)
        {
            RestoreMainPanel();
            mainPanel.SetActive(false);
            newGameNamingTab.SetActive(true);
            newGameNamingTab.GetComponent<NewGameCreationController>().Init(selectedSlot);
        }

        public void Load(int sloatNumber) => Load(SaveManager.Instance.ExtractSaveFile(sloatNumber));

        public void Load(SaveFile saveFile)
        {
            if (saveFile is null)
                return;
            var createdObject = new GameObject("InformationPasser");
            createdObject.AddComponent<InformationPasser>().Init(saveFile);
            SceneManager.LoadScene("LoadingScene");
        }
    }
}
