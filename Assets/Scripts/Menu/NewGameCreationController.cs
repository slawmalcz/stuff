using Save;
using UnityEngine;

namespace Menu
{
    public class NewGameCreationController : MonoBehaviour
    {
        public InputPanelController inputController = null;
        public int selectedSlot = 0;

        public void Init(int selectedSlot) => this.selectedSlot = selectedSlot;

        public void OnConfirm() => inputController.mainMenu.Load(SaveManager.Instance.CreteNewSave(inputController.inputTextField.text, selectedSlot));
    }
}
