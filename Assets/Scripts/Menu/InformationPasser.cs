﻿using Assets.Scripts.Loading;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class InformationPasser : MonoBehaviour
    {
        private SaveFile fileToLoad = null;

        public void Init(SaveFile data)
        {
            DontDestroyOnLoad(gameObject);
            fileToLoad = data;
            SceneManager.sceneLoaded += OnSceneLoad;
        }

        private void OnSceneLoad(Scene arg0, LoadSceneMode arg1)
        {
            FindObjectOfType<LoadingSceneController>().Init(fileToLoad);
            SceneManager.sceneLoaded -= OnSceneLoad;
            Destroy(gameObject);
        }
    }
}
