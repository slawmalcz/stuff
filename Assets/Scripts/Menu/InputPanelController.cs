using Menu;
using TMPro;
using UnityEngine;

namespace Menu
{
    public class InputPanelController : MonoBehaviour
    {
        public MenuControllers mainMenu;
        public TextMeshPro inputTextField = null;

        private bool isActionInproggers = false;
        private const string AVIABLE_SYMBOLS = "QWERTYUIOPASDFGHJKLZXCVBNM234567890qwertyuioopasdfghjklzxcvbnm ";

        void Update()
        {
            if (Input.anyKey)
            {
                if (Input.GetKey(KeyCode.Backspace) && !isActionInproggers)
                {
                    inputTextField.text = inputTextField.text.Substring(0, inputTextField.text.Length - 1);
                    isActionInproggers = true;
                }
                else if (Input.GetKey(KeyCode.Escape) && !isActionInproggers)
                    mainMenu.RestoreMainPanel();
                else
                {
                    foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode)))
                    {
                        if (Input.GetKey(vKey) && !isActionInproggers && AVIABLE_SYMBOLS.Contains(vKey.ToString()))
                        {
                            inputTextField.text += vKey.ToString();
                            isActionInproggers = true;
                        }
                    }
                }
            }
            else
            {
                isActionInproggers = false;
            }
        }
    }
}
